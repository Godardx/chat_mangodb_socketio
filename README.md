# Installation projet

--- 

## Étape 1 : 

Il faudra clonner le projet en utilisant la commande `git clone` et en mettant l'URL du projet à la suite de la commande.
`git clone https://gitlab.com/Godardx/chat_mangodb_socketio.git` <-- Comme ceci

---

## Étape 2 : 

Une fois le projet installé il faudra l'ouvrir dans n'importe quel IDE (VSCode recommandé)

---

## Étape 3 : 

Une fois le projet ouvert il faudra le lancer via deux commande à effectuer l'une après l'autre

- `npm i`
- `npm start`

---

## Étape 4 : 

Et pour finir vous pourrez lancer le serveur sur votre navigateur internet via l'adresse suivante : `localhost/3000` 
