const { count } = require('console');

const app = require('express')();
const http = require('http').createServer(app);
const io = require('socket.io')(http)

const { MongoClient } = require('mongodb');

// Logs pour la connexion à la base de données

const uri = 'mongodb+srv://thomas:thomasadmin@cluster0.recsy.mongodb.net/chat?retryWrites=true&w=majority';
const client = new MongoClient(uri);

// Connexion à la base de données

client.connect((err, db) => {
    if (err) {

        // Tentative de connexion à la base de données échoué

        process.exit(1);
    } else {

        // Tentative de connexion à la base de données réussite

        let dbo = db.db('chat');
        console.log("Base de données connecté")
        let connectedUsers = 0;

        let stockedMessages = [];

        app.get('/', (req, res) => {
            res.sendFile(__dirname + '/index.html');
        });

        // Lancement du serveur sur le port définit

        http.listen(3000, () => {
            console.log('Le serveur est prêt');
        });

        // Connection d'un utilisateur

        io.on('connection', (socket) => {
            console.log('Utilisateur connecté')
            io.emit('chat message', `Un utilisateur vient de se connecter`);

            dbo.collection("message").find({}, { projection: { _id: 0 } }).toArray(function(err, result) {
                if (err) throw err;
                for (let i = 0; i < result.length; i++) {
                    console.log(result.length);
                    console.log(result[i].message);
                    socket.emit('stocked messages', result[i].message);
                }

            })
            console.log(stockedMessages);
            connectedUsers++;

            // Déconnection d'un utilisateur

            socket.on('disconnect', () => {
                console.log('Utilisateur deconnecté');
                io.emit('chat message', `Un utilisateur vient de se déconnecter`);
                connectedUsers--;
                countUsers(connectedUsers);

            });
            countUsers(connectedUsers);

            // Zone d'écriture de message

            socket.on('chat message', (msg) => {
                io.emit('chat message', msg);
                let obj = { message: msg }
                dbo.collection('message').insertOne(obj, function(err, res) {
                    if (err) throw err;
                    console.log("Insérer un message")
                })
            });
        })
    }
})